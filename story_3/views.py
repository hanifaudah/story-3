from django.shortcuts import render
from django.forms import Form

# Models
from story_3.models import Message

general_context = {
  'ig_icon': '/static/story_3/instagram.svg',
  'line_icon': '/static/story_3/line.svg',
  'gmail_icon': '/static/story_3/gmail.svg',
}

def profil(request):
  context = {
    'pic_path': 'static/story_3/personal_pic.jpg',
    **general_context
  }
  return render(request, 'story_3/index.html', context)

def blog(request):
  notif = ''
  if (request.method == 'POST'):
    form = MessageForm(request.POST)
    if form.is_valid():
      user = request.POST['user']
      message = request.POST['message']
      new_message = Message(user=user, message=message)
      new_message.save()
      notif = 'Success!'
  else:
    form = MessageForm()

  context = {
    'form': form,
    'notif': notif,
    'messages': Message.objects.all(),
    **general_context
  }
  return render(request, 'story_3/blog.html', context)

class MessageForm(Form):
  class Meta:
    model = Message
    fields = ['user', 'message']



